# Guía de Instalación
## Portal SENASBA

## 1. Instalación Sistema Operativo.

El sistema operativo es Debian 9 que se puede descargar de: [https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/]

Para instalarlo en modo consola para servidores siga los siguientes pasos [https://www.comoinstalarlinux.com/como-instalar-debian-como-servidor/]

## 2. Instalación de paquetes necesarios.

Una vez instalados el sistema base, instalaremos los siguientes paquetes:

```
$ sudo apt update
$ sudo apt install apache2 sqlite3 php-sqlite3 curl php libapache2-mod-php php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip

```

## 3. Configuración Apache2.

Para la configuración de Apache2 debemos crear un archivo de la siguiente manera:

```
$ sudo nano /etc/apache2/sites-available/wordpress.conf

```

Y debemos pegar lo siguiente dentro de archivo creado, para un ambiente con SSL

```
<VirtualHost *:80>
  ServerName dev.3600.lat/wordpress/

  RewriteEngine On
  RewriteCond %{HTTPS} off
  RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
</VirtualHost>
<IfModule mod_ssl.c>
<VirtualHost *:443>
  ServerName dev.3600.lat/wordpress/

  DocumentRoot /var/www/dev.3600.lat/wordpress/

  <Directory /var/www/dev.3600.lat/wordpress>
    Options  Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order allow,deny
    allow from all
        Require all granted
  </Directory>

  SSLEngine on
  Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains;"
  SSLCertificateFile    /etc/letsencrypt/live/dev.3600.lat/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/dev.3600.lat/privkey.pem
</VirtualHost>
</IfModule>
```

Habilitamos el sitio:

```
$ sudo a2ensite wordpress.conf
```

Verificamos que el modulo rewrite esté habilitado

```
$ sudo a2enmod rewrite
```

Y reiniciamos apache

```
$ sudo service apache2 restart
```

### Configurando límite de memoria de PHP y Wordpress

1. Para configurar el límite de memoria de PHP buscamos en el archivo php.ini la línea que dice:

```
memory_limit = 128M
```

y la modificamos por:

```
memory_limit = 256M
```

y guardamos.

2. Para modificar el límite de menoria dentro de Wordpress editamos el archivo wp-config.php, e insertamos la siguiente línea:

```
define( 'WP_MEMORY_LIMIT', '256M' );
```

justo antes de la línea que dice:

```
/* That's all, stop editing! Happy blogging. */
```

y guardamos.

3. Finalmente reiniciamos el servicio de apache:

```
$ sudo service apache2 restart
```