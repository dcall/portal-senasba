# Portal SENASBA
## Información general

Desarrollo del portal WEB del Servicio Nacional para la Sostenibilidad de Servicios en Saneamiento Básico.

## 1. Sistema Base

Para el sistema base del ambiente de desarrollo se utilizó Debian 9 que es una Distribución Libre y hoy por hoy la más estable.

### 1.1. Paquetes Instalados en Debian

Se instalaron y configuraron los paquetes necesarios para el correcto funcionamiento del portal sobre el sistema base (Debian 9), los paquetes base instalados son Apache2 con SSL, PHP 7.0, SQLite 3. Para más detalles ver la guía de Instalación.

## 2. Base de Datos

Se optó por utilizar SQLite 3, un gestor de base de datos liviano y facil de gestionar (copias de respaldo en un solo archivo)

## 3. Content Manager System (CMS)

El Sistema Administrador de Contenidos (CMS por sus siglas en Inglés) elegido para el desarrollo del proyecto fué WordPress 4.9.8, se utilizará el núcleo de éste sistema así como complementos (Plugins) y temas (Themes) de tipo gratuito para generar nuestro portal.

### 3.1. Plugins

#### 3.1.1. Sql Lite Integrator

Plugin que permite a WordPress utilizar SQLite como base de datos.

#### 3.1.2. Advanced Custom Fields

Plugin que permite a WordPres generar campos personalizados al momento de generar nuevas entradas.

#### 3.1.3. My Theme Login

Plugin que genera automáticamente las páginas "Acceder" y "Cerrar Sesión".

#### 3.1.4. PDF Embedder

Plugin que pertime la visualización directamente en un post.

### 3.2. Temas

Se probaron varios temas preconfigurados de WordPress, buscando uno que sea responsivo y contenga las secciones que la guía de implementación de sitios web que AGETIC dicta.

#### 3.2.1. Personalización del Temas

Con el fin de personalizar el Tema a los lineamientos de AGETIC se utilizó CSS adicional incrustado en la sección "CSS Adicional" de la Personalización del Tema.

## 4. Contenidos

Los contendidos del portal se estructuraron primero en categorías y estructuras de menus de acuerdo a lo normado por AGETIC, tomando en cuenta principalmente las secciones de: Ciudadanía, Gobierno, Prensa y Transparencia.

(En esta parte puedes introducir la matriz que se envío a SENASBA)