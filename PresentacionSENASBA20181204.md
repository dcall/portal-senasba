# PRESENTACIÓN PORTAL WEB SENASBA

## 1. Sistema Operativo.

Debian 9

## 2. Sistema de Administración de Contenidos.

WordPress 4.9.8

## 3. Tecnologías Wordpress

PHP 7, Jquery, Ajax, Json

## 4. Base de Datos.

SQLite 3

## 5. Contenidos.

- Entradas (Contenido dinámico)
- Páginas (Contenido estático)
- Categorías
- Etiquetas (Palabras Clave)
- Menus
- Campos personalizados por categoría

## 6. Roles

- Suscriptor
- Colaborador
- Autor
- Editor
- Administrador

## 7. PDF

- Incrustados para páginas.
- Visibles mediante link para entradas.

## 8. Guía de Instalación.

https://gitlab.com/dcall/portal-senasba

## 9. Manual de Usuario.

En desarrollo.

## 10. Usuarios de Prueba

- Administrador SENASBA (administrador)
- Editor SENASBA (editor)
- Autor SENASBA (autor)
- Colaborador SENASBA (colaborador)
